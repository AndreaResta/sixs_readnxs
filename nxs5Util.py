#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 09:48:00 2024

@author: andrea
"""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
#from __future__ import unicode_literals
import h5py   ### h5py should take its place
import os
import numpy as np
import pickle
import time
import datetime
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm


############# to help debug
from inspect import currentframe, getframeinfo
from inspect import currentframe

def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno

class rn5(object):
    def __init__(self,ReasNxs5_Obj, verbose=False):
        '''Receive an ReadNxs5 object and all its attributes, it adds functions. '''
        
        for el in ReasNxs5_Obj.__dict__.keys():
            self.__dict__[el] =  ReasNxs5_Obj.__getattribute__(el)
        self.version = [self.version, 'n5u 30/07/2024']
        self.verbose = verbose
        
    def save2txt(self, list2exp):
        '''It saves a txt file with the variable listed.
        the variable must be present in the "attlist" attribute
        it expect a list of strings. 
        It dos not export images
        '''
        filesave = self.filename[:-4] + '_extr.txt'
        longname = os.path.join(self.directory,filesave)
        exported = []
        outdata = np.empty(len(self.__getattribute__(list2exp[0])))    
        for el in list2exp:
            if el in self.attlist:
                if len(np.shape(self.__getattribute__(el))) > 1 and (self.verbose == 'YES'):
                       print('attribute ',el, 'has more that one dimension' )
                if len(np.shape(self.__getattribute__(el))) == 1:
                    try:
                        loc = self.__getattribute__(el)
                        outdata = np.c_[outdata, loc]
                        exported.append(el)
                    except:
                        if self.verbose: raise 
                                   
        headerline = '   '.join(exported)            
        np.savetxt(longname, outdata[:,1:], delimiter = '\t', header=headerline)
        
    def calcROIs(self, stack, ROIs, mask, detname):
        '''get the ROIs from the det_ROIs
        Multiply the det matix for the mask (if present)
        calculate the integrated intensity on each ROIs
        divide by the acquisition time, if present'''
        
        self.calc_attenuations()
        for pos, roi in enumerate(ROIs): # read each
            print(roi)
            numname = detname + '_roi' + str(pos) + '_'
            integratedIntensity = None
            try:
                integratedIntensity = self.roi_sum_mask(stack,roi,mask)
                #print(integratedIntensity.shape, get_linenumber())
                numname = numname + 'm'
                if hasattr(self,'integration_time'):
                    numname = numname + 't'
                    integratedIntensity = integratedIntensity/self.integration_time
            except:
                if self.verbose: print('calcROIs mask@ ',get_linenumber())
                try:
                    integratedIntensity = self.roi_sum(stack,roi)
                    numname = numname
                except:
                    if self.verbose: print('calcROIs raw integration@ ',get_linenumber())
                pass
            if integratedIntensity is not None:
                self.__dict__[numname]  = integratedIntensity*self.fattenuations
            else:
                if self.verbose: print('calcROIs@ ',get_linenumber())
                   
                   
                    
                    #print('rois is: ', roi,' n: ', pos)
                    
                    
                    
    def calc_attenuations(self):
        '''It aims to calculate: (attcoef**filters[:])) considering the presence of att_new and old '''
        if not hasattr(self,'attcoef'): 
            self.attcoef = False
        if not hasattr(self,'attcoef_old'):
            self.attcoef_old = False
        if not hasattr(self,'attenuation_old'):
            self.attenuation_old = False
        if not hasattr(self,'attenuation'):
            self.attenuation = False
        try:                                   ## this attempt must be first!! 
            self.fattenuations = (self.attcoef**self.attenuation)
            list.append(self.attlist, 'fattenuations')
        except:
            if self.verbose: print('attenuation @ ',get_linenumber())
        try:                                  ## this attempt must be second!! 
            self.fattenuations = (self.attcoef**self.attenuation)*(self.attcoef_old**self.attenuation_old)
        except:
             if self.verbose: print('attenuation @ ',get_linenumber())
        return

    def roi_sum(self, stack,roi):
        '''given a stack of images it returns the integals over the ROI  
        roi is expected as eg: [257, 126,  40,  40] '''
         
        return stack[:,roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2]].sum(axis=1).sum(axis=1).copy()   

    def roi_sum_mask(self, stack,roi,mask):
        '''given a stack of images it returns the integals over the ROI minus 
        the masked pixels  
        the ROI is expected as eg: [257, 126,  40,  40] 
        the ROI is expected as eg: [257, 126,  40,  40]'''

        _stack = stack[:]*(1-mask.astype('uint16'))
        return _stack[:,roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2]].sum(axis=1).sum(axis=1)             
        
        
    def find_att_name(self, str_name):
        '''Given a partial attribute name it return all the attributes 
        names that contains that string 
        '''
        ret_list = []
        for el in self.__dict__.keys():
            if str_name in el:
                list.append(ret_list, el)
        return ret_list
    
    def calcRelTime(self):
        try:
            if hasattr(self,'epoch'):
                self.RelTime = self.epoch-min(self.epoch)
                list.append(self.attlist, 'RelTime') 
        except:
            if self.verbose: print('calcROIs mask@ ',get_linenumber())
                
            
            
    
        
        