#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generated from the MaskTool2 at the beamline (SixS soleil).
meant to be used to generate masks from nxs files
either in the BM enviroment or standalone.
"""
try:
    import sixs_nxsread.ReadNxs4 as ReadNxs4
except:
    pass
try:
    import ReadNxs4 
except:
    pass
import numpy as np
import time
import matplotlib.pyplot as plt
#plt.ion()
import os
#import scipy as sp
from matplotlib.colors import LogNorm
class mask:

    def __init__(self, recordingmanager=None,config_pub=None,detector = 'xpad140'):#attribute='xpad'):
        '''USE:
        generate the mask object : mask = MaskTool.mask(recordingmanager, config_pub)
        calculate the mask       : mask.maskgen1()
        upload and save the mask : mask.upldMask()'''
        self.version = '16/12/2022'
        self.__version__ = self.version
        self._BMenv = False
        self._recordingmanager = recordingmanager
        self._config_pub = config_pub
        #self._attribute = attribute
        self.detector = detector
        self._mask_dict = {}
        self._imgs = None
        self.mask = None
        self._current_nxs_filename = None
        
        
        
               
        ####################################################      
        ###### functions loaded only in the BM environment
        if config_pub is not None:
            self._BMenv = True  # the presence of the object config_pub also define the BMenv (BM environment) varialble
            if self._config_pub.mask is None:
                self._config_pub.ismask = False

            def on(self):
                self._config_pub.ifmask = True
        
            def off(self):
                self._config_pub.ifmask = False
        
        #if self._BMenv:
            def nxs_reload(self):
                '''Reload the mast used nxs file '''
                self._nxs_read(self._config_pub.mask_from_file)
                return
            def info(self):
                print (self._config_pub.mask_file)
                print (self._config_pub.mask_from_file)
                if self._config_pub.mask is not None:
                    print(np.sum(self._config_pub.mask),' pixels removed')
                    plt.imshow(self._config_pub.mask,interpolation='nearest', norm=LogNorm())
                    plt.show()
                else:
                    print('No mask')
                return()
            def mask_reload(self):
                '''Reload the last used mask. '''
                self.mask_load(self._config_pub.mask_file)
            
            def push(self):
                '''Push the just generated mask from make into the publisher and save it amongs the data.  '''
                if self.mask is None:
                    print ('No mask')
                    return
                else:
                    self._config_pub.mask_from_file = self._current_nxs_filename
                    directory =self._config_pub.mask_from_file [:self._config_pub.mask_from_file.rfind('/')]
                    svn =self._savename()
                    self._config_pub.mask_file = os.path.join(directory,svn)
                    np.save(self._config_pub.mask_file, self.mask)
                    self._config_pub.mask = self.mask
                    print (svn, 'saved in: ', directory)
        
    ######################################################
    #### more general functions            
    print('line 91')
    def _add_grid(self,m):
        print(m.shape) 
        if not m.shape[1]%80+m.shape[0]%120 :   # test pour XPAD
            mx = m.shape[1]-1
            m[:,79:mx:80]=1
            m[:,80:mx:80]=1
            my = m.shape[0]-1       
            m[119:my:120,:]=1
            m[120:my:120,:]=1
        return m

    def _corr_double(self,img):
        coef=2.59
        if not img.shape[1]%80+img.shape[0]%120 :  # test pour XPAD
            mx = img.shape[1]-1
            img[:,79:mx:80]/=coef
            img[:,80:mx:80]/=coef
            my = img.shape[0]-1       
            img[my-1,:]/=coef
            img[my,:]/=coef
        return img
        
    def _savename(self):
        '''Generate the mask saving name'''
        tt=time.localtime()
        if self._current_nxs_filename is None:
            number = '00000'
        else:
            number = self._current_nxs_filename.split('_')[-1].split('.')[0]
        return '%s%s_%04d%02d%02d_%02dh%02d.npy' % ('mask_nxs',number,tt[0],tt[1],tt[2],tt[3],tt[4])

    def _nxs_read(self, filename):
        '''Read the file and extract the images matrix
        '''
        self._nxs = ReadNxs4.DataSet(filename)
        try :
            if len(self._nxs._list2D)==1:
                self._imgs = self._nxs.__dict__[self._nxs._list2D[0]]
            if len(self._nxs._list2D)>1:
                string4input = 'Multiple detectors, choose one: ' + '; '.join(self._nxs._list2D)
                choice = input(string4input)
                self._imgs = self._nxs.__dict__[choice]
            if len(self._nxs._list2D)<1:
                print('No recognised 2D detector in the selected file')    
        except:
            raise 
            
        self._current_nxs_filename = filename
        return

    def nxs_load(self, filename = None):
        '''If filename is not specified it loads the last scan measured.'''
        if filename is not None:
            self.filename =  filename
        else:
            if self.BMenv:
                self.directory = self._recordingmanager.projectpath + os.sep + self._recordingmanager.datasubdirectory
                self.filename = os.path.join(self.directory,self._recordingmanager.lastfilename)
        self._nxs_read( self.filename )
        return    

    def show(self):
        '''It allow you to select and print loaded images or masks '''
        if self._imgs is None :
            pass
        else:
            ans =  input('Plot image number : ')
            if self.isInteger(ans):
                ans = int(ans)
                plt.imshow(self._imgs[ans],interpolation='nearest',norm=LogNorm())
                plt.show()
            if not self.isInteger(ans):
                print('Answer must be an integer')
            
        for key in iter(self._mask_dict):
            #M = self._mask_dict[key]
            ans =  input('Plot '+key[1:] +' image (y/n) ?')
            if ans == 'y':
                plt.imshow(self.__dict__[key],interpolation='nearest')
                plt.show()
            ans =  input('Plot mask from '+key[1:] +' (y/n) ?')
            if ans == 'y':
                plt.imshow(self._mask_dict[key],interpolation='nearest')
                plt.show()
        if self.mask is None :
            pass
        else:
            ans =  input('Plot mask (y/n) ?')
            if ans == 'y':
                plt.imshow(self.mask,interpolation='nearest')
                plt.show()
        if self._BMenv:
            if self._config_pub.mask is None :
                pass
            else:
                ans =  input('Plot mask from config (y/n) ?')
                if ans == 'y':
                    plt.imshow(self._config_pub.mask,interpolation='nearest')
                    plt.show()

    def make(self):
        '''It allow you do linear combination of masks generated with different methods and makes it available for the push command.'''
        if self._imgs is None :
            print ('Verify nxs File and detector ????')
            return

        self.mask = np.zeros_like(self._imgs[0]) # first image as template for sizes
        for key in iter(self._mask_dict):
            M = self._mask_dict[key]
            ans =  input('Use mask from '+key[1:]+' ('+str(M.sum())+'pix removed) (y/n) ?')
            if ans == 'y':
                self.mask = np.logical_or(self.mask, M)
        print (' Tolal of reloved pix =', self.mask.sum())
        plt.imshow(self.mask,interpolation='nearest')
        plt.show()

    def mask_load(self, MaskFileName):
        '''Load a specific '.npy' file for the  mask. '''
        self.mask = np.load(MaskFileName)
        if self._BMenv: # apply BM environment
            self._config_pub.mask = self.mask
            self._config_pub.mask_file =  MaskFileName


    def tool_mean(self, first=None, last=None, xmax=None):
        '''Methode to generate the mask
        Meant to be applied on a timescan on a rather flat signal region.
        Discrimination on the aboslute count rate of each pixel.'''
        if self._imgs is None:
            print('WARNING : ... need a nxsfile ...')
            return
        else:
            label = '_mean'
            if first and last :
                self.__dict__[label ] = self._imgs[first:last].sum(0)/(last-first)
            else:
                self.__dict__[label ] = self._imgs.mean(0)
            y, x, = np.histogram(self.__dict__[label ], bins=1000, range=(0,3*self.__dict__[label ].mean()))
            if xmax :
                lims = (0,xmax)
            else:
                xmax = x[y.argmax()]*5.
                lims = (0,xmax)
            y, x, = np.histogram(self.__dict__[label ], bins=1000, range=lims)

            plt.plot((x[1:]+x[:-1])/2., y)

            print ('click on the low and high threshold from the graph and close the plot window' )
            print ('Please click on the low threshold')
            Min = plt.ginput()[0][0]
            print('clicked at', Min)
            print ('Please click on the high threshold')
            Max = plt.ginput()[0][0]
            print('clicked at', Max)
            plt.ion()
            plt.show()
            plt.close()
            m1 = np.where(self.__dict__[label] < Min, 1,0)
            m2 = np.where(self.__dict__[label] > Max, 1,0)
            self.__dict__[label+'_limits'] = (Min,Max)
            self._mask_dict[label] = self._add_grid(np.logical_or(m1, m2))
            print(np.sum(self._mask_dict[label]),' pixels removed')
            plt.imshow(self._mask_dict[label],interpolation='nearest')
            plt.show()
        return

    def tool_mean2(self, first=None, last=None, xmax=None):
        '''Methode to generate the mask
        Meant to be applied on a timescan on a rather flat signal region.
        Discrimination on the aboslute count rate of each pixel. It accounts 
        for the different surface area of the junction pixels.
        USE ONLY FOR XPADS'''
        if self._imgs is None:
            print('WARNING : ... need a nxsfile ...')
            return
        else:
            label = '_mean2'
            if first and last :
                self.__dict__[label ] = self._imgs[first:last].sum(0)/(last-first)
            else:
                self.__dict__[label ] = self._imgs.mean(0)

            self._corr_double(self.__dict__[label ])
            if xmax :
                lims = (0,xmax)
            else:
                lims = None
            y, x, = np.histogram(self.__dict__[label ], bins=1000, range=lims)
            
            
            plt.plot((x[1:]+x[:-1])/2., y)

            print ('click on the low and high threshold from the graph and close the plot window' )
            print ('Please click on the low threshold')
            Min = plt.ginput()[0][0]
            print('clicked at', Min)
            print ('Please click on the high threshold')
            Max = plt.ginput()[0][0]
            print('clicked at', Max)
            plt.ion()
            plt.show()
            plt.close()
            m1 = np.where(self.__dict__[label] < Min, 1,0)
            m2 = np.where(self.__dict__[label] > Max, 1,0)
            self.__dict__[label+'_limits'] = (Min,Max)
            self._mask_dict[label] = (np.logical_or(m1, m2))
            print(np.sum(self._mask_dict[label]),' pixels removed')
            plt.imshow(self._mask_dict[label],interpolation='nearest')
            plt.show()
        return


    def tool_sigma(self, first=None, last=None, xmax=None):
        '''Methode to generate the mask
        Meant to be applied on a timescan on a rather flat signal region.
        Discrimination on the Standard deviation for each pixel.'''
        if self._imgs is None:
            print('WARNING : ... need a nxsfile ...')
            return
        else:
            label = '_sigma'
            if first and last :
                mean = self._imgs[first:last].sum(0)/(last-first)
                imgs = self._imgs[first:last]
            else:
                mean = self._imgs.mean(0)
                imgs = self._imgs

            sig2 = np.zeros_like(self._imgs[0]) * 1.
            
            for img in imgs:
                sig2 += (img - mean)**2
            self.__dict__[label ] = np.sqrt(sig2)

            if xmax :
                lims = (0,xmax)
            else:
                lims = None
            y, x, = np.histogram(self.__dict__[label ], bins=1000, range=lims)
            plt.plot((x[1:]+x[:-1])/2., y)

            print ('click on the low and high threshold from the graph and close the plot window' )
            print ('Please click on the low threshold')
            Min = plt.ginput()[0][0]
            print('clicked at', Min)
            print ('Please click on the high threshold')
            Max = plt.ginput()[0][0]
            print('clicked at', Max)
            plt.ion()
            plt.show()
            plt.close()
            m1 = np.where(self.__dict__[label] < Min, 1,0)
            m2 = np.where(self.__dict__[label] > Max, 1,0)
            self.__dict__[label+'_limits'] = (Min,Max)
            self._mask_dict[label] = self._add_grid(np.logical_or(m1, m2))
            print(np.sum(self._mask_dict[label]),' pixels removed')
            plt.imshow(self._mask_dict[label],interpolation='nearest')
            plt.show()
        return

    def tool_rectangle(self):
        '''It place an ROI on the mask.
        ROI is ([70,240,0,363]) row_start, row_end, col_start, col_end
        '''
        if self._imgs is None:
            print('WARNING : ... need a nxsfile ...')
            return
        else:
            i_mean = self._imgs.mean(0)
            label = '_rectangle'
            plt.ion()
            plt.show()
            plt.imshow(i_mean,interpolation='nearest')
            print('Select top left and bottom righ corners of the rectangle.')
            TopLeft = np.around(np.asanyarray(plt.ginput()))
            print('clicked at', TopLeft[0][0], TopLeft[0][1])
            BottomRight = np.around(np.asanyarray(plt.ginput()))
            print('clicked at', BottomRight[0][0], BottomRight[0][1])
            plt.close()
            
            mout=np.ones(np.shape(i_mean), dtype=bool)
            mout[TopLeft[0][1]:BottomRight[0][1],TopLeft[0][0]:BottomRight[0][0]]=False
            mout = self._add_grid(mout)
            plt.imshow(i_mean*(1-mout), interpolation = 'nearest')
            plt.show()
            ans =  input('Are u Happy? ....(y)')
            if ans =='y' or ans =='':
                self._mask_dict[label] = mout
                print(np.sum(self._mask_dict[label]),' pixels removed')
                plt.close()
                return
            if ans !='y' and ans!='':
                plt.close()
                return self.tool_rectangle()

    def tool_circle(self):
        '''It selects a circle with central pixel at center=([a,b])
        and radius = R expressed in pixels'''
        if self._imgs is None:
            print('WARNING : ... need a nxsfile ...')
            return
        else:
            i_mean = self._imgs.mean(0)
            label = '_rectangle'
            plt.ion()
            plt.show()
            plt.imshow(i_mean,interpolation='nearest')
            print('Select center and radius of the circle.')
            center = np.around(np.asanyarray(plt.ginput()))
            print('clicked at', center[0][0], center[0][1])
            rad_click = np.around(np.asanyarray(plt.ginput()))
            print('clicked at', rad_click[0][0], rad_click[0][1])
            plt.close()
            radius = np.sqrt((center[0][0]-rad_click[0][0])**2+(center[0][1]-rad_click[0][1])**2)

            mout=np.ones(np.shape(i_mean), dtype=bool)
            for i in range(mout.shape[0]):
                for j in range(mout.shape[1]):
                    if (i-center[0][1])**2 + (j-center[0][0])**2 < radius**2:
                        mout[i,j] = False

            mout = self._add_grid(mout)
            plt.imshow(i_mean*(1-mout),interpolation='nearest')
            plt.show()
            ans =  input('Are u Happy? ....(y)')
            if ans =='y' or ans =='':
                self._mask_dict[label] = mout
                print(np.sum(self._mask_dict[label]),' pixels removed')
                plt.close()
                return
            if ans !='y' and ans!='':
                plt.close()
                return self.tool_circle()
            
    def saveMask(self):
        svn = self._savename()
        longname = self._nxs.directory + svn
        np.save(longname, self.mask)
            
######################################################
#### Others useful functions
    def isInteger(self, num):
        isInt = True
        try:
            int(num)
        except:
            isInt = False
        return isInt
    
        
