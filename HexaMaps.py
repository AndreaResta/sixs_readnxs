#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  7 14:40:41 2024

@author: andrea
A useful set of functions to retrive  data from nxs files and plor a x-y map of scattered values.

version 0.01

"""

import h5py
import os
import numpy as np
import glob
import matplotlib.pyplot as plt


############# to help debug
from inspect import currentframe, getframeinfo
from inspect import currentframe

def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno

###############################################################################
###### functions to deal with data sources/files
def isFloat(string):
    '''Return True if the input can be interpreted as Float'''
    try:
        float(string)
        return True
    except ValueError:
        return False

def get_scan_number(file):
    "Return scan number of nexus file for SixS"
    file = os.path.basename(file)

    return int(os.path.splitext(file)[0].split("_")[-1])


def get_file_range(directory, start_number, end_number, pattern='*.nxs'):
    """
    Select a file range depending on the numeration at the end of the file name,
    
    return: list of file names
    """
    print("Using as directory:", directory)
    all_file_list = sorted(
        glob.glob(directory + pattern),
        key=os.path.getmtime,
    )

    # Filter based on scan number
    file_list = []
    for file in all_file_list:
        file_number = get_scan_number(file)
        if start_number <= file_number <= end_number and os.path.isfile(file):
            file_list.append(os.path.basename(file))
    print(f"Found {len(file_list)} file(s).")
    return file_list

def number2file(num, directory,extension = '*.nxs',splt = '_'):
    '''Given a number it returns the corresponding scan in the specified folder.'''

    dirtotal = directory
    li = glob.glob1(dirtotal,extension)
    for el in li:
        elnumber = el[:-4].split(splt)[-1]
        if isFloat(elnumber):
            number = float(elnumber)
            if number == num:
                return  el    

def load_nexus_attribute(key, file, directory=None):
    """
    Get attribute values from nexus file in f.com.scan_data

    :param key: attribute name (str)
    :param file: file name (str)
    :param directory: path to directory (str)

    return: content of attribute
    """
    if not isinstance(key, str):
        print("param 'key' must be a string")
        return None

    if isinstance(directory, str):
        path_to_nxs_data = os.path.join(directory, file)
    else:
        path_to_nxs_data = file

    if not os.path.isfile(path_to_nxs_data):
        print("param 'path_to_nxs_data' must be a valid path to a hdf5 file:",
              path_to_nxs_data)
        return None

    try:
        with h5py.File(path_to_nxs_data, "r") as f:
            return f[f"com/scan_data/{key}"][()]
    except OSError:
        print(
            f"path_to_nxs_data: {path_to_nxs_data} is not a valid NeXuS file")
        return None
    except KeyError:
        print(f"key: {key} was not found in f.com.scan_data")
        return None

###############################################################################
###### functions for pixel detector maps

def map_pixdet(
    file_list,
    directory=None,
    map_type="hexapod_scan",
    roi_key=None,
    x_key=None,
    y_key=None,
    verbose=False,
    compute_roi=False,
    scale="log",
    flip_axes=False,
    shading="nearest",
    cmap='turbo',
    square_aspect=True,
):
    """
    Extract map from a list of scans.
    The intensity data will be referred to the pixel detector
    :param file_list: list of .nxs files in param directory
    :param directory: directory in which file_list are, default is None
    :param map_type: str in ("hexapod_scan", "pi_map") , pi_map is for future
     used to give default values for params roi_key, x_key and y_key
    :param roi_key: str, key used in f.root.com.scan_data for roi
     default value depends on 'map_type'
    :param x_key: str, key used in f.root.com.scan_data for x
     default value depends on 'map_type'
    :param y_key: str, key used in f.root.com.scan_data for y
     default value depends on 'map_type'
    :param verbose: True for more details
    :param compute_roi: container of len 4, ROI
    :param scale: str in ("log", "lin")
    :param flip_axes: True to switch x and y axis
    :param shading: use 'nearest' for no interpolation and 'gouraud' otherwise
    :param cmap: colormap to use for the map
    :param square_aspect: True to force square aspect in the final plot
    :param rotation_angle: angle in degrees to rotate the plot

    return: Error or success message
    return also: arr_x, arr_y, plotted_array
    """
    # Check parameters
    if len(file_list) == 0:
        return ("`file_list` parameter must be of length at least 1")

    if map_type not in ("hexapod_scan", "pi_map"):
        return ("`map_type` parameter must be 'hexapod_scan' or 'pi_map'")

    if not isinstance(roi_key, str):
        if map_type == "hexapod_scan":
            roi_key = "roi1_merlin"
            print("Defaulted 'roi' to 'roi1_merlin'")
        elif map_type == "pi_map":
            roi_key = "roi1_merlin"
            print("Defaulted 'roi' to 'roi1_merlin'")
    else:
        print("Using for roi:", roi_key)

    if not isinstance(x_key, str):
        if map_type == "hexapod_scan":
            x_key = "X"
            print("Defaulted 'x' to 'X'")
        elif map_type == "pi_map":
            x_key = "pi_x"
            print("Defaulted 'x' to 'pi_x'")
    else:
        print("Using for x:", x_key)

    if not isinstance(y_key, str):
        if map_type == "hexapod_scan":
            y_key = "Y"
            print("Defaulted 'y' to 'Y'")
        elif map_type == "pi_map":
            y_key = "pi_y"
            print("Defaulted 'y' to 'pi_y'")
    else:
        print("Using for y:", y_key)

    if not isinstance(verbose, bool):
        return ("`verbose` parameter must be a Boolean")

    if scale not in ("log", "lin"):
        return ("`scale` parameter must be 'lin' or 'log'")

    if not isinstance(flip_axes, bool):
        return ("`flip_axes` parameter must be a Boolean")

    # Save file range index, specific to SixS naming
    first_scan = get_scan_number(file_list[0])
    last_scan = get_scan_number(file_list[-1])

    # Get data
    X_list, Y_list, roi_sum_list = [], [], []

    # Load data
    for file in file_list:
        x = load_nexus_attribute(key=x_key, file=file, directory=directory)
        y = load_nexus_attribute(key=y_key, file=file, directory=directory)
        roi_sum = load_nexus_attribute(
            key=roi_key, file=file, directory=directory)
        
        if compute_roi:
            a, b, c, d = compute_roi
            roi_sum = np.sum(roi_sum[:, a:b, c:d], axis = (1, 2))

        # Append to lists
        X_list.append(x)
        Y_list.append(y)
        roi_sum_list.append(roi_sum)
        if verbose:
            print(
                f"Loading :{file}"
                f"\n\tShape in x: {x.shape}"
                f"\n\tShape in y: {y.shape}"
                f"\n\tShape in roi_sum: {roi_sum.shape}\n"
            )

    # Create empty arrays
    arr_roi = np.zeros((len(file_list), len(Y_list[0])))
    arr_y = np.zeros((len(file_list), len(Y_list[0])))
    arr_x = np.zeros((len(file_list), len(Y_list[0])))

    # Fill arrays
    for j, (x, y, roi_sum) in enumerate(zip(X_list, Y_list, roi_sum_list)):
        # y is constant, x changes
        if verbose:
            print(f"Scan nb {j} for y=", np.round(np.mean(y), 3))
        arr_roi[j] = roi_sum
        arr_y[j] = np.around(y, 3)
        arr_x[j] = np.around(x, 3)

    #if map_type == "ascan_y":
    #    # Flip because the scans go with increasing y every other scan
    #    # and otherwise decreasing y
    #    for i in range(len(arr_y)):
    #        if i % 2 == 1:
    #            arr_y[i] = np.flip(arr_y[i])
    #            arr_roi[i] = np.flip(arr_roi[i])

    # Apply scale
    if scale == 'lin':
        plotted_array = arr_roi
    else:
        plotted_array = np.where(arr_roi > 0, np.log10(arr_roi), 0)

    # Plot data
    title = f"Scans {first_scan} ----> {last_scan}"
    plot_mesh(
        arr_x,
        arr_y,
        plotted_array,
        title,
        cmap=cmap,
        flip_axes=flip_axes,
        shading=shading,
    )

    # return ("Data successfully plotted.")
    return arr_x, arr_y, plotted_array

###############################################################################
###### functions for fluo detector maps
def plot_spectras_fluo(num, directory, fluo_key=None, eVstep = 10.01, spectras = [0,-1],xaxe = 'eV'):
    """ 
    :param num      : int, nxs file numner
    :param directory: str, folder full path 
    :param fluo_key : str, key used in f.root.com.scan_data for fluo
    :param eVstep   : float, energy step 
    :param spectras : list, [10,12] plots spectras from 10 to 12. otherwise it plots all.
    :param spectras : str, 'eV' for an x-axe in eV or 'pos' for an x-axe array-position
    """
    if not isinstance(fluo_key, str):
        # this need to be modified for the fluo plots
        fluo_key = "xia_vortexchannel00"
        print("Defaulted XRF channel00")
    
    fn = number2file(num, directory)
    fluo_spectras = load_nexus_attribute(
        key=fluo_key, file=fn, directory=directory)
    (fluo_spectras.shape)
    if xaxe =='eV': 
        xax = np.arange(fluo_spectras.shape[1])*eVstep
        plt.xlabel('energy eV')
    if xaxe == 'pos':
        xax = np.arange(fluo_spectras.shape[1])
        plt.xlabel('array Position')
    sst = spectras[0]
    snd = spectras[1]    
    for el in fluo_spectras[sst:snd]:
        plt.plot(xax, el)
        

    plt.ylabel('intensity')
    plt.minorticks_on()
    plt.grid(which='major', linestyle='-', linewidth='0.5', color='black')  
    plt.grid(which='minor', linestyle=':', linewidth='0.5', color='blue')
    
    
    
    return


def map_fluo(
    file_list,
    directory=None,
    map_type="hexapod_scan",
    fluo_key=None,
    x_key=None,
    y_key=None,
    verbose=False,
    compute_fluoROI=False,
    scale="log",
    flip_axes=True,
    shading="nearest",
    cmap='turbo',
    square_aspect=True,
):
    """
    Extract map from a list of scans
    :param file_list: list of .nxs files in param directory
    :param directory: directory in which file_list are, default is None
    :param map_type: str in ("hexapod_scan", "ascan_y"),
     used to give default values for params fluo_key, x_key and y_key
    :param fluo_key: str, key used in f.root.com.scan_data for roi
     default value depends on 'map_type'
    :param x_key: str, key used in f.root.com.scan_data for x
     default value depends on 'map_type'
    :param y_key: str, key used in f.root.com.scan_data for y
     default value depends on 'map_type'
    :param verbose: True for more details
    :param compute_fluoROI: container of len 2, Start and End positions of the spectra rage to integrate e.g.: [800,900] 
    :param scale: str in ("log", "lin")
    :param flip_axes: True to switch x and y axis
    :param shading: use 'nearest' for no interpolation and 'gouraud' otherwise
    :param cmap: colormap to use for the map
    :param square_aspect: True to force square aspect in the final plot
    :param rotation_angle: angle in degrees to rotate the plot

    return: Error or success message
    return also: arr_x, arr_y, plotted_array
    """
    # Check parameters
    if len(file_list) == 0:
        return ("`file_list` parameter must be of length at least 1")

    if map_type not in ("hexapod_scan", "ascan_y"):
        return ("`map_type` parameter must be 'hexapod_scan' or 'ascan_y'")

    if not isinstance(fluo_key, str):
        # this need to be modified for the fluo plots
        if map_type == "hexapod_scan":
            fluo_key = "xia_vortexchannel00"
            print("Defaulted XRF channel00")
        elif map_type == "pi_map":
            fluo_key = "xia_vortexchannel00"
            print("Defaulted 'fluo' to 'xia_vortexchannel00'")
    else:
        print("Using for fluo:", fluo_key)

    if not isinstance(x_key, str):
        if map_type == "hexapod_scan":
            x_key = "X"
            print("Defaulted 'x' to 'X'")
        elif map_type == "pi_map":
            x_key = "pi_x"
            print("Defaulted 'x' to 'pi_x'")
    else:
        print("Using for x:", x_key)

    if not isinstance(y_key, str):
        if map_type == "hexapod_scan":
            y_key = "Y"
            print("Defaulted 'y' to 'Y'")
        elif map_type == "pi_map":
            y_key = "pi_y"
            print("Defaulted 'y' to 'pi_y'")
    else:
        print("Using for y:", y_key)

    if not isinstance(verbose, bool):
        return ("`verbose` parameter must be a Boolean")

    if scale not in ("log", "lin"):
        return ("`scale` parameter must be 'lin' or 'log'")

    if not isinstance(flip_axes, bool):
        return ("`flip_axes` parameter must be a Boolean")

    # Save file range index, specific to SixS naming
    first_scan = get_scan_number(file_list[0])
    last_scan = get_scan_number(file_list[-1])

    # Get data
    X_list, Y_list, fluo_sum_list = [], [], []

    # Load data
    for file in file_list:
        x = load_nexus_attribute(key=x_key, file=file, directory=directory)
        y = load_nexus_attribute(key=y_key, file=file, directory=directory)
        fluo_sum = load_nexus_attribute(
            key=fluo_key, file=file, directory=directory)
        
        
        if compute_fluoROI:
            st = compute_fluoROI[0]
            nd = compute_fluoROI[1]
            fluo_sum = np.sum(fluo_sum[:, st:nd], axis = (1)) # partial integral of the fluo spectra
        else:
            fluo_sum = np.sum(fluo_sum, axis = (1)) # total integral of the fluo-spectra

        # Append to lists
        X_list.append(x)
        Y_list.append(y)
        fluo_sum_list.append(fluo_sum)
        if verbose:
            print(
                f"Loading :{file}"
                f"\n\tShape in x: {x.shape}"
                f"\n\tShape in y: {y.shape}"
                f"\n\tShape in fluo_sum: {fluo_sum.shape}\n"
            )

    # Create empty arrays
    arr_fluo = np.zeros((len(file_list), len(Y_list[0])))
    arr_y = np.zeros((len(file_list), len(Y_list[0])))
    arr_x = np.zeros((len(file_list), len(Y_list[0])))

    # Fill arrays
    for j, (x, y, fluo_sum) in enumerate(zip(X_list, Y_list, fluo_sum_list)):
        # y is constant, x changes
        if verbose:
            print(f"Scan nb {j} for y=", np.round(np.mean(y), 3))
        arr_fluo[j] = fluo_sum
        arr_y[j] = np.around(y, 3)
        arr_x[j] = np.around(x, 3)

    if map_type == "ascan_y":
        # Flip because the scans go with increasing y every other scan
        # and otherwise decreasing y
        for i in range(len(arr_y)):
            if i % 2 == 1:
                arr_y[i] = np.flip(arr_y[i])
                arr_fluo[i] = np.flip(arr_fluo[i])

    # Apply scale
    if scale == 'lin':
        plotted_array = arr_fluo
    else:
        plotted_array = np.where(arr_fluo > 0, np.log10(arr_fluo), 0)

    # Plot data
    title = f"Scans {first_scan} ----> {last_scan}"
    plot_mesh(
        arr_x,
        arr_y,
        plotted_array,
        title,
        cmap=cmap,
        flip_axes=flip_axes,
        shading=shading,
    )

    # return ("Data successfully plotted.")
    return arr_x, arr_y, plotted_array
###############################################################################
###### common functions for plot


def plot_mesh(
    arr_x,
    arr_y,
    plotted_array,
    title=None,
    cmap='jet',
    flip_axes=False,
    shading="nearest",
    square_aspect=True,
):
    """
    Plot mesh of values from x and y coordinates.
    Axis are forced to be square

    :param arr_x: positions in x, shape (X,)
    :param arr_y: positions in y, shape (Y,)
    :param plotted_array: values, 2D array of shape (X, Y)
    :param cmap: colormap used for mesh
    :param flip_axes: True to switch x and y
    :param shading: use 'nearest' for no interpolation and 'gouraud' otherwise
    :param square_aspect: True to force square aspect in the final plot
    :param rotation_angle: angle in degrees to rotate the plot
    """

    # Define figure
    fig, ax = plt.subplots(figsize=(8, 8))

    if flip_axes:
        im = ax.pcolormesh(
            arr_y,
            arr_x,
            plotted_array,
            cmap=cmap,
            shading=shading,
        )
    else:
        im = ax.pcolormesh(
            arr_x,
            arr_y,
            plotted_array,
            cmap=cmap,
            shading=shading,
        )

    if square_aspect:
        ax.axis('square')
    if flip_axes:
        ax.set_xlabel('y (mm)')
        ax.set_ylabel('x (mm)')
    else:
        ax.set_xlabel('x (mm)')
        ax.set_ylabel('y (mm)')

    if isinstance(title, str):
        ax.set_title(title)

    fig.colorbar(im, orientation='vertical')
    ax.grid()


    # Show figure
    plt.tight_layout()
    plt.show(block=False)
