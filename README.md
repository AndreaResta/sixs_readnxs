02/04/21

Those files were writen with the intent to read and open the data produced at SixS beamline Soleil.

NxsRead4 read a nxs file and return an object containing most of the data.
NxsRead4 make use of the alias_dict file that is dictionary containing most of the "device_name ==> Human_redeable names".
In absence of alias_dict it generates some less self explained names.

there are some useful function for data recuctions such as recalculates the ROIs and correct by the attenuation factors or to plot some simple curves/detector images

the nxs2spec3 script makes use of the object generated by NxsRead4 to generate a "spec" file from all the nxs files in a specified folder. The spec fill will containing most of the information useful for easy data overview.

to run it on a linux machine from a terminal:
python3 nxs2spec3 /path/to/folder/


